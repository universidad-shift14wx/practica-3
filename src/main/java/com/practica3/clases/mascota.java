package com.practica3.clases;

import javafx.beans.property.SimpleStringProperty;

public class mascota {

    private SimpleStringProperty name;
    private SimpleStringProperty telefono;
    private SimpleStringProperty fecha;
    private SimpleStringProperty raza;
    private SimpleStringProperty especie;
    private SimpleStringProperty peso;
    private SimpleStringProperty sexo;
    private SimpleStringProperty Color;
    private SimpleStringProperty comentarios;

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getTelefono() {
        return telefono.get();
    }

    public SimpleStringProperty telefonoProperty() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }

    public String getFecha() {
        return fecha.get();
    }

    public SimpleStringProperty fechaProperty() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha.set(fecha);
    }

    public String getRaza() {
        return raza.get();
    }

    public SimpleStringProperty razaProperty() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza.set(raza);
    }

    public String getEspecie() {
        return especie.get();
    }

    public SimpleStringProperty especieProperty() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie.set(especie);
    }

    public String getPeso() {
        return peso.get();
    }

    public SimpleStringProperty pesoProperty() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso.set(peso);
    }

    public String getSexo() {
        return sexo.get();
    }

    public SimpleStringProperty sexoProperty() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo.set(sexo);
    }

    public String getColor() {
        return Color.get();
    }

    public SimpleStringProperty colorProperty() {
        return Color;
    }

    public void setColor(String color) {
        this.Color.set(color);
    }

    public String getComentarios() {
        return comentarios.get();
    }

    public SimpleStringProperty comentariosProperty() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios.set(comentarios);
    }

    public mascota(SimpleStringProperty name, SimpleStringProperty telefono, SimpleStringProperty fecha, SimpleStringProperty raza, SimpleStringProperty especie, SimpleStringProperty peso, SimpleStringProperty sexo, SimpleStringProperty color, SimpleStringProperty comentarios) {
        this.name = name;
        this.telefono = telefono;
        this.fecha = fecha;
        this.raza = raza;
        this.especie = especie;
        this.peso = peso;
        this.sexo = sexo;
        Color = color;
        this.comentarios = comentarios;
    }


}
