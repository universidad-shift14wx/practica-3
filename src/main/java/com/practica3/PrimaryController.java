package com.practica3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.*;
import animatefx.animation.*;
import com.jfoenix.controls.*;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class PrimaryController implements Initializable {

    @FXML
    Label usuarios;
    @FXML
    Label contrasena;
    @FXML
    Button inicioSesion;
    @FXML
    JFXButton btnClose = new JFXButton();
    @FXML
    JFXButton btnRegisto = new JFXButton();
    @FXML
    JFXTextField txtUser;
    @FXML
    JFXPasswordField txtPass;

    @FXML
    StackPane stackDialog;



    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }

    @FXML
    private void animateUsuarioText() throws InterruptedException {
    new Shake(usuarios).play();
    }

    @FXML
    private void animateContrasenaText() throws InterruptedException {
        new Shake(contrasena).play();
    }

    @FXML
    private void animateButton() throws InterruptedException, ExecutionException, IOException {
        if(!this.txtUser.getText().isEmpty() && !this.txtPass.getText().isEmpty()){
            if(this.txtUser.getText().equals("user") && this.txtPass.getText().equals("123")){
                new Flip(inicioSesion).play();
                GoTo esperarAirAotraPantalla = new GoTo("mascotas");
                new Thread(esperarAirAotraPantalla).start();
            }else{
                this.showModalError();
            }
        }else{
            this.showModalError();
        }
    }

    public void showModalError() throws IOException {
        this.stackDialog.setVisible(true);
        JFXDialogLayout content = new JFXDialogLayout();
        FXMLLoader Formulario = new FXMLLoader(getClass().getResource("/com/practica3/errores/simpleErrorLoginDialogBody.fxml"));
        content.setHeading(new Text("Error"));
        AnchorPane bod = Formulario.load();
        content.setBody(bod);
        JFXDialog dialog=new JFXDialog(stackDialog, content, JFXDialog.DialogTransition.CENTER);
        btnClose.setText("Cerrar");
        btnClose.setStyle("-fx-background-color: #dc3545");
        btnClose.setStyle("-fx-color-label-visible: white");
        content.setActions(btnClose);
        btnClose.setOnMouseClicked(e->{
            dialog.close();
            this.stackDialog.setVisible(false);
        });
        dialog.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        new Shake(btnRegisto).setDelay(Duration.seconds(1)).play();
        this.stackDialog.setVisible(false);
    }
}

class GoTo extends Task<Integer>{
    String gotoir;

    public GoTo(String gotoir) {
        this.gotoir =gotoir;
    }

    @Override
    protected Integer call() throws Exception {
        System.out.println("awaiting");
        Thread.sleep(1500);
        App.setRoot(this.gotoir);
        return 10;
    }
}



