package com.practica3;

import animatefx.animation.FadeInLeft;
import animatefx.animation.FadeOutLeft;
import animatefx.animation.Shake;
import com.jfoenix.controls.*;
import com.practica3.clases.mascota;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class MascotasFormularioController implements Initializable {


    @FXML
    Label faltan;

    @FXML
    public JFXButton btnAgregar;
    @FXML
    public JFXButton btnClose;
    @FXML
    JFXTextField nombre;
    @FXML
    JFXTextField telefono;
    @FXML
    DatePicker fecha;
    @FXML
     JFXTextField raza;
    @FXML
    JFXTextField especie;
    @FXML
    JFXTextField peso;
    @FXML
    JFXComboBox<String> sexo;
    @FXML
    JFXTextField Color;
    @FXML
    JFXTextArea comentarios;


    public ObservableList<String> sexos = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        new FadeOutLeft(this.faltan).play();
        this.sexos.setAll("Masculino","femenino");
        this.sexo.setItems(sexos);
    }

    public boolean esValido() {
        if(!this.nombre.getText().isEmpty()
                && !this.Color.getText().isEmpty()
                && !this.comentarios.getText().isEmpty()
                && !this.peso.getText().isEmpty()
                && !this.raza.getText().isEmpty()
                && !this.especie.getText().isEmpty()
                && !this.telefono.getText().isEmpty()
        ){
            return true;
        }else{
            new FadeInLeft(this.faltan).play();
            new Shake(this.faltan).setDelay(Duration.millis(1500L)).play();
            return false;
        }

    }

    public mascota getMascota(){
        return new mascota(
                new SimpleStringProperty(this.nombre.getText()),
                new SimpleStringProperty(this.telefono.getText()),
                new SimpleStringProperty(this.fecha.getValue().toString()),
                new SimpleStringProperty(this.raza.getText().toString()),
                new SimpleStringProperty(this.especie.getText().toString()),
                new SimpleStringProperty(this.peso.getText().toString()),
                new SimpleStringProperty(this.sexo.getValue().toString()),
                new SimpleStringProperty(this.Color.getText().toString()),
                new SimpleStringProperty(this.comentarios.getText().toString())
        );
    }
}
