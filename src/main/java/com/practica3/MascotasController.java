package com.practica3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import animatefx.animation.FadeInLeft;
import animatefx.animation.FadeOutLeft;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.practica3.clases.mascota;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class MascotasController implements Initializable {

    @FXML
    StackPane stackDialog;
    @FXML
    JFXButton btnMenu;
    @FXML
    AnchorPane drawer;


    @FXML public TableView<mascota> mascotaTableView = new TableView<mascota>();
    @FXML public TableColumn<mascota,String> tcnombre;
    @FXML public TableColumn<mascota, String> tctelefono;
    @FXML public TableColumn<mascota,String> tcfecha;
    @FXML public TableColumn<mascota,String> tcraza;
    @FXML public TableColumn<mascota,String> tcespecie;
    @FXML public TableColumn<mascota,String> tcpeso;
    @FXML public TableColumn<mascota,String> tcsexo;
    @FXML public TableColumn<mascota,String> tccolor;
    @FXML public TableColumn<mascota,String> tccomentarios;

    ObservableList<mascota> listMascotas = FXCollections.observableArrayList();

    boolean drawerInOut=true;

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }

    void setTable(){
        this.tcnombre.setCellValueFactory(nombre->nombre.getValue().nameProperty());
        this.tctelefono.setCellValueFactory(telefono->telefono.getValue().telefonoProperty());
        this.tcfecha.setCellValueFactory(fech->fech.getValue().fechaProperty());
        this.tcraza.setCellValueFactory(raza->raza.getValue().razaProperty());
        this.tcespecie.setCellValueFactory(especie->especie.getValue().especieProperty());
        this.tcpeso.setCellValueFactory(peso->peso.getValue().pesoProperty());
        this.tcsexo.setCellValueFactory(sexo->sexo.getValue().sexoProperty());
        this.tccolor.setCellValueFactory(color->color.getValue().colorProperty());
        this.tccomentarios.setCellValueFactory(comentarios->comentarios.getValue().comentariosProperty());
        this.mascotaTableView.setItems(this.getListProductos());
    }

    public ObservableList<mascota> getListProductos(){
        return this.listMascotas;
    }

    @FXML
    public void showMenu(){
        if (drawerInOut){
            new FadeInLeft(drawer).play();
        }else{
            new FadeOutLeft(drawer).play();
        }

        drawerInOut = !drawerInOut;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.setTable();
        new FadeOutLeft(drawer).play();
        this.stackDialog.setVisible(false);
    }

    @FXML
    public void AgregarMascota() throws IOException {
        this.stackDialog.setVisible(true);
        System.out.println("cargando");
        JFXDialogLayout content = new JFXDialogLayout();
        FXMLLoader Formulario = new FXMLLoader(getClass().getResource("/com/practica3/mascotas/MascotaFormularioUI.fxml"));
        content.setHeading(new Text("Agregar Mascota"));
        AnchorPane formularioAnchor = Formulario.load();
        MascotasFormularioController formularioController = Formulario.getController();
        content.setBody(formularioAnchor);
        System.out.println("sin problemas");
        JFXDialog dialog=new JFXDialog(stackDialog, content, JFXDialog.DialogTransition.CENTER);
        dialog.show();
        formularioController.btnAgregar.setOnMouseClicked(e->{
            if (formularioController.esValido()){
                mascota newMascota = formularioController.getMascota();
                this.listMascotas.add(newMascota);
                dialog.close();
                this.stackDialog.setVisible(false);
            }else{

            }
        });

        formularioController.btnClose.setOnMouseClicked(e->{
           dialog.close();
           this.stackDialog.setVisible(false);
        });

    }
}