module com.practica3 {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.jfoenix;
    requires io.reactivex.rxjava2;
    requires AnimateFX;
    opens com.practica3 to javafx.fxml;
    exports com.practica3;
}